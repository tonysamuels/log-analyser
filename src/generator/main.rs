use std::error::Error;
use std::fs::OpenOptions;
use std::io::{BufWriter, Write};

use bytesize::ByteSize;

use clap::{App, Arg};

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("JSON generator")
        .version("1.0")
        .author("Tony S. <tony@samuels.me.uk>")
        .about("Generates JSON logs to test Log Analyser against")
        .arg(
            Arg::with_name("filename")
                .help("the FILE to write to, note this will refuse to overwrite an existing file")
                .index(1)
                .required(true),
        )
        .arg(
            Arg::with_name("length")
                .short("l")
                .long("length")
                .default_value("1K")
                .help("File length, in bytes or similar (e.g., 1K 234M 2G)"),
        )
        .arg(
            Arg::with_name("type_length")
                .short("t")
                .long("type-length")
                .default_value("30")
                .help("Length of types, in bytes or similar (e.g., 1K 234M 2G)"),
        )
        .get_matches();

    let filename = matches.value_of_os("filename").unwrap();
    let length: ByteSize = matches.value_of("length").unwrap().parse()?;
    let type_length: ByteSize = matches.value_of("type_length").unwrap().parse()?;
    let type_length = type_length.as_u64() as usize;

    let file = OpenOptions::new()
        .create_new(true)
        .write(true)
        .open(filename)?;
    let mut writer = BufWriter::new(file);

    let mut bytes_added = 0u64;

    while bytes_added < length.as_u64() {
        let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .map(char::from)
            .take(type_length)
            .collect();

        bytes_added +=
            writer.write(format!(r#"{{"type":"{}"}}{}"#, rand_string, '\n').as_bytes())? as u64;
    }

    writer.flush()?;

    Ok(())
}
