use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::Error as IoError;

use bytesize::ByteSize;

use serde_json::error::Error as JsonError;

#[derive(Debug)]
pub enum Error {
    /// Failed to open the file
    Open(IoError),
    /// Failed to read the file
    Read(IoError),
    /// Failed to write to standard out
    Write(IoError),
    /// JSON parsing failed
    Json(JsonError),
    /// Integer conversion failed. This should not be possible on the majority of systems
    /// at time of writing, as it's converting from usize to u64
    IntConv,
    /// No input data provided, so no processing can be done
    NoData,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        use Error::*;
        match self {
            Open(e) => write!(f, "Can't open file: {}", e),
            Read(e) => write!(f, "Can't read from file: {}", e),
            Json(e) => write!(f, "Invalid JSON: {}", e),
            Write(e) => write!(f, "Failed to write to stdout: {}", e),
            IntConv => write!(
                f,
                "Input data too large, it exceeds {} bytes ({}) for at least one data type",
                u64::MAX,
                ByteSize(u64::MAX)
            ),
            NoData => write!(f, "No logs in input"),
        }
    }
}
