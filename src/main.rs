use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};

mod config;
mod error;

use config::Settings;
use error::Error;

use bytesize::ByteSize;

use either::Either;

use hashbrown::HashMap;

use serde::Deserialize;

use tabwriter::TabWriter;

/// The data we need to keep for each type read from FILE.
#[derive(Debug, Default)]
struct TypeData {
    /// The number of bytes on all the lines for the type represented by this `struct`.
    byte_len: usize,
    /// The number of occurances of the type represented by this `struct`.
    count: usize,
}

impl TypeData {
    /// Creates a new instance of `TypeData` for a given line.
    /// As this assumes that a line exists, the count starts at 1.
    fn new(byte_len: usize) -> Self {
        // The line must have been at least `{"type":""}`, which is 11 bytes long.
        debug_assert!(byte_len > 11);
        Self { byte_len, count: 1 }
    }

    fn add_line(&mut self, byte_len: usize) {
        // The line must have been at least `{"type":""}`, which is 11 bytes long.
        debug_assert!(byte_len > 11);
        self.byte_len += byte_len;
        self.count += 1;
    }
}

fn main() {
    if let Err(e) = run(&Settings::from_env()) {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}

/// Wrapper function to allow control over the printing of the Error type, without
/// forcing the Debug implementation to be the same as Display.
fn run(settings: &Settings) -> Result<(), Error> {
    // Predeclare to ensure lifetimes.
    let stdin;
    let file;

    // Determine whether to read from stdin or a file.
    let mut reader = if settings.filename == "-" {
        stdin = std::io::stdin();
        Either::Left(BufReader::new(stdin.lock()))
    } else {
        file = File::open(&settings.filename).map_err(Error::Open)?;
        Either::Right(BufReader::new(file))
    };

    let mut type_set = HashMap::new();
    let mut line = Vec::new();
    // Read until there are no lines left.
    while reader.read_until(b'\n', &mut line).map_err(Error::Read)? > 0 {
        add_line_to_map(&line, &mut type_set)?;
        // Clear out the line, this does not deallocate storage, letting
        // use reuse the same memory for each line.
        line.clear();
    }

    if type_set.is_empty() {
        Err(Error::NoData)
    } else {
        write_table(type_set, settings)
    }
}

/// Adds the data from the line to the map. If there is already an entry in the map
/// with the right type then add to that, otherwise create a new entry.
fn add_line_to_map(line: &[u8], map: &mut HashMap<String, TypeData>) -> Result<(), Error> {
    let byte_len = line.len();
    let line_type = extract_type(line)?;

    // This would be cleaner using `Entry`s, however they require a `String` key for every
    // access, in case an insertion is required. It's not worth the overhead of creating
    // a new buffer for every access, so we handle the insertion ourselves.
    if let Some(data) = map.get_mut(line_type) {
        data.add_line(byte_len);
    } else {
        map.insert(line_type.to_owned(), TypeData::new(byte_len));
    }

    Ok(())
}

/// Outputs a table whose format depends on the given `settings`.
fn write_table(type_set: HashMap<String, TypeData>, settings: &Settings) -> Result<(), Error> {
    let stdout = std::io::stdout();
    let handle = stdout.lock();
    let writer = BufWriter::new(handle);

    let mut writer = if settings.pretty {
        Either::Left(TabWriter::new(writer))
    } else {
        Either::Right(writer)
    };

    if !settings.headless {
        writeln!(&mut writer, "Type\tCount\tBytes").map_err(Error::Write)?;
    }

    for (t, d) in type_set {
        write!(&mut writer, "{}\t", t).map_err(Error::Write)?;
        write!(&mut writer, "{}\t", d.count).map_err(Error::Write)?;

        // If human readable output has been requested, then use the ByteSize to format
        // the output of the byte count.
        if settings.human_readable {
            // ByteSize requires a u64. On most modern OS this is a guaranteed safe
            // conversion, however it's possible that usize is u128 or larger, in which case
            // the conversion may be able to fail.
            writeln!(
                &mut writer,
                "{}",
                ByteSize(d.byte_len.try_into().map_err(|_| Error::IntConv)?)
            )
            .map_err(Error::Write)?;
        } else {
            writeln!(&mut writer, "{}", d.byte_len).map_err(Error::Write)?;
        }
    }

    writer.flush().map_err(Error::Write)
}

/// Given a line as bytes, convert the value of the type to a `&str`.
///
/// Also checks that the line is valid JSON and returns an error if not.
fn extract_type(json: &[u8]) -> Result<&str, Error> {
    #[derive(Deserialize)]
    struct Type<'a> {
        #[serde(rename = "type")]
        value: &'a str,
    }

    serde_json::from_slice::<Type>(json)
        .map(|t| t.value)
        .map_err(Error::Json)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn verify_type_extraction() {
        let input = r#"{"type":"A"}"#;
        let extracted_type = extract_type(input).unwrap();
        assert_eq!(extracted_type, "A");

        let input = r#"{"type":"B", "misc_data": "C"}"#;
        let extracted_type = extract_type(input).unwrap();
        assert_eq!(extracted_type, "B");

        let input = r#"{"misc_data": "D", "type":"E"}"#;
        let extracted_type = extract_type(input).unwrap();
        assert_eq!(extracted_type, "E");
    }
}
