use std::ffi::OsString;

use clap::{App, Arg};

/// Settings read at startup
#[derive(Debug, Default)]
pub struct Settings {
    /// Human readable byte sizes
    pub human_readable: bool,
    /// Make the output table prettier at the cost of increased processing
    pub pretty: bool,
    /// Don't print a header on the table
    pub headless: bool,
    /// The path of the file to output to, or `-` if we should output to standard output
    ///
    /// As system paths may not be valid UTF-8, use an OsString
    pub filename: OsString,
}

impl Settings {
    /// Fetch settings from the current environment
    pub fn from_env() -> Self {
        let matches = App::new("Log Analyser")
            .version("1.0")
            .author("Tony S. <tony@samuels.me.uk>")
            .about(
                "Analyses JSON log messages from a FILE, printing the frequency and size of va\
rious log types on the standard output.\n\nWhen FILE is -, read standard input.",
            )
            .arg(
                Arg::with_name("filename")
                    .help("the FILE to parse")
                    .index(1)
                    .required(true),
            )
            .arg(
                Arg::with_name("human")
                    .short("h")
                    .long("human-readable")
                    .help("print human readable sizes (e.g., 1K 234M 2G)"),
            )
            .arg(
                Arg::with_name("pretty")
                    .short("p")
                    .long("pretty-print")
                    .help("prettier formatting: ensures column alignment in table"),
            )
            .arg(
                Arg::with_name("headless")
                    .long("no-header")
                    .help("prints without the header"),
            )
            .get_matches();

        Self {
            human_readable: matches.is_present("human"),
            pretty: matches.is_present("pretty"),
            headless: matches.is_present("headless"),
            // Safe to unwrap as it's a mandatory argument, `get_matches` will have
            // exited already if this would not succeed.
            filename: matches.value_of_os("filename").unwrap().to_owned(),
        }
    }
}
