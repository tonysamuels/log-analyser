# log analyser (la)

log anaylser is a tool for analysing JSON formatted log messages, providing a table for each provided type of message, showing the total number of messages of that type and how many bytes all those messages sum to.

## Input format

Input can be provided by file or standard input. Either way the input format MUST be such that every line is an individual log, which MUST be a valid object JSON, which MUST contain a `type` field. e.g.
```JSON
{"type": "hex", "data": "0x56421413246d54f1564"}
{"type": "object", "object": {"data": true}}
```

There is no limit on the `type` the JSON objects must have, although all input MUST be valid JSON, which includes the requirement to be valid UTF-8.

## Output format

log anaylser supports 3 different output format toggles, which can be used in any combination. The default output format consists of a header, and a line per provided log type. Each line contains the type the line matches to, the number of occurences of that log, and the number of bytes contained in those logs. Example output:
```
Type    Count   Bytes
A       3000000 42000000
E       1000000 27000000
B       1000000 27000000
```
`-h --human-readable` Prints the byte count using units. NB: This can be expected to have around a 10% performance impact. Example output:
```
Type    Count   Bytes
A       3000000 42.0 MB
E       1000000 27.0 MB
B       1000000 27.0 MB
```
`-p --pretty` Prints the output, ensuring reasonable aligment of columns and spacing between columns. NB: This can be have a very large performance impact. Example output:
```
Type  Count    Bytes
A     3000000  42000000
E     1000000  27000000
B     1000000  27000000
```
`--no-header` Prints the output without a header. Example output:
```
A       3000000 42000000
E       1000000 27000000
B       1000000 27000000
```

## Installation instructions

Cargo has a convient install command you can run from the root directory of the repository. This will install to `~/.cargo/bin/la` unless configured otherwise, make sure this path is in your `PATH`.

```bash
cargo install --path . --bin la
````

## Limitations

- In order to calculate the number of bytes for each type of log, all log types are stored in memory, alongside with some metadata. If there are a lot of log types, or those log types are themselves very long, then there is a risk that the computer may run out of memory.
- The number of bytes and instance count of each log type are stored in a native word sized integer. It is possible this may overflow if there is a lot of provided data.
- When using `--human-readable` the number of bytes for each type is converted into a 64 bit unsigned number. It is possible for this conversion to fail if a massive amount of logs are written on a system with a native word size greater than 64 bits.
- When using `--pretty` in order to properly align columns in the output, the entire output table is stored in memory. Even if the types and associated metadata are able to be kept in memory, it is possible for that amount to be exceeded here, causing an out of memory failure.
